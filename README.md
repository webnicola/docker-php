php

## Test
`cd 7.4`

`docker build -t test-php:7.4 .`

`docker run --env APACHE_DOCUMENT_ROOT=/var/www/drupal/web --publish 8000:80 --detach --name test-php test-php:7.4`

## Create a Drupal project

composer create-project drupal/recommended-project my_site_name_dir

This will create a project in 'my_site_name_dir' and automatically executes composer install to download the latest stable version of Drupal and all its dependencies.

Your 'my_site_name_dir' will contain files that should be outside of your web root and not  accessible by the web server. The web root will be 'my_site_name_dir/web'.